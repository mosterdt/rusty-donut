/**
 * 
 */
package worthyShower;

import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.SocketException;
import java.net.UnknownHostException;
import java.util.Random;

import worthyShower.DHCPClient.ClientState;

/**
 * @author Thomas De Backer
 * @author Anthony Clays
 *
 */
public class ColdSunshine {

	/**
	 * The main method.
	 *
	 * @param args the arguments
	 * @throws SocketException the socket exception
	 */
	@SuppressWarnings("unused")
	public static void main(String[] args) throws SocketException {
        DatagramSocket socket = new DatagramSocket(1337);
        DHCPServer server = new DHCPServer(socket);
        new Thread(server).start();
        
        InetAddress ip = null;
        int port = 0;
		try {
			if (false) {
				ip = InetAddress.getByAddress(new byte[] { (byte) 10, (byte) 33, (byte) 14, (byte) 246 });
				port = 1234;
			} else if (true) {
				ip = InetAddress.getLocalHost();
				port = 1337;
			} else if (false) {
				ip = InetAddress.getByAddress(new byte[] { (byte) 10, (byte) 33, (byte) 14, (byte) 56 });
				port = 12001;
			}
		} catch (UnknownHostException e) {
		}
		
		//floodServer(ip, port);
		
		byte[] mac1 = new byte[] {(byte)0xde, (byte)0xfa, (byte) 0xce, (byte)0xde, (byte)0xfa, (byte)0xce};
		byte[] mac2 = new byte[] {(byte)0xee, (byte)0xfa, (byte) 0xce, (byte)0xde, (byte)0xfa, (byte)0xce};
		
		DHCPClient client1 = new DHCPClient(ip, port, mac1);
        new Thread(client1).start();
        try {Thread.sleep(10000);} catch (InterruptedException e) {	}
        DHCPClient client2 = new DHCPClient(ip, port, mac2);
        new Thread(client2).start();
        
        try {Thread.sleep(30000);} catch (InterruptedException e) {	}
        client1.setState(ClientState.DYING);
        try {Thread.sleep(65000);} catch (InterruptedException e) {	}
        client1.setState(ClientState.INITREBOOT);
	}
	
	public static void floodServer(InetAddress ip, int port){
		Random rand = new Random();
		for (int i = 0; 260 > i; i++) {
			byte[] mac = DHCPPacket.longToBytes(rand.nextLong());
			DHCPClient client = new DHCPClient(ip, port, mac);
			new Thread(client).start();
			//try {Thread.sleep(1000);} catch (InterruptedException e) {	}
		}
	}

}
