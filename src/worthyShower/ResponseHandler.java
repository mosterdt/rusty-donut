package worthyShower;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;

public class ResponseHandler implements Runnable {

	private DatagramPacket recv;
	private DatagramSocket socket;

	public ResponseHandler(DatagramSocket socket, DatagramPacket recv) {
		this.socket = socket;
		this.recv = recv;
	}

	/**
	 * Handle.
	 *
	 * @param receivedPacket the received packet
	 * @return the DHCP packet
	 */
	private DHCPPacket handle(DHCPPacket receivedPacket) {
		if (receivedPacket.getDHCPType() == DHCPOption.DHCPMessageType.DHCPDISCOVER) {
			System.out.println("                                          Server: replying");
			receivedPacket.setOp(DHCPPacket.OpcodeType.BOOTREPLY);
			receivedPacket.setHtype(DHCPPacket.HWAddressType.Ethernet);
			receivedPacket.setHlen(6);
			receivedPacket.setHops(0);
			// xid already set
			receivedPacket.setSecs(0);
			// flags already set
			receivedPacket.setCiaddr(0);
			receivedPacket.setYiaddr(0xC0A81401);
			receivedPacket.setSiaddr(0);
			// giaddr already set
			// chaddr already set
			receivedPacket.setSname(new byte[64]);
			receivedPacket.setFile(new byte[128]);
			receivedPacket.resetOptions();

			DHCPOption leaseTime = new DHCPOption(DHCPOption.DHCPOptionType.IPAddressLeaseTime);
			leaseTime.setData(20);
			receivedPacket.addOption(leaseTime);

			DHCPOption message = new DHCPOption(DHCPOption.DHCPOptionType.Message);
			message.setData("yolo".getBytes());
			receivedPacket.addOption(message);

			DHCPOption serverIdentifier = new DHCPOption(DHCPOption.DHCPOptionType.ServerIdentifier);
			serverIdentifier.setData(this.recv.getAddress().getAddress());
			receivedPacket.addOption(serverIdentifier);

			DHCPOption reqOption = new DHCPOption(DHCPOption.DHCPOptionType.DHCPMessage);
			reqOption.setData(DHCPOption.DHCPMessageType.DHCPOFFER.getID());
			receivedPacket.addOption(reqOption);

		} else {
			System.out.println("                                          Server: echo");
		}
		return receivedPacket;
	}

	@Override
	public void run() {
		System.out.println("                                          Server: server thread started!");
		byte[] data = this.recv.getData();
		DHCPPacket receivePacket = new DHCPPacket(data);
		System.out.println("                                          Server: received " + receivePacket.getDHCPType());
		DHCPPacket sendPacket = handle(receivePacket);
		data = sendPacket.toBytes();
		DatagramPacket send = new DatagramPacket(data, data.length, this.recv.getAddress(), this.recv.getPort());

		try {
			System.out.println("                                          Server: sent " + sendPacket.getDHCPType());
			this.socket.send(send);
		} catch (IOException e) {
			System.out.println("why u so difficult");
			e.printStackTrace();
		}

	}
}
