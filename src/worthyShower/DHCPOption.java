/*
 * 
 */
package worthyShower;

/**
 * The Class DHCPOption.
 */
public class DHCPOption {
	
	/**
	 * The Enum DHCPMessageType.
	 */
	public enum DHCPMessageType {
		// Server to client with configuration parameters,
		// including committed network address.
		DHCPACK(5),
		
		// Client to server indicating network address is already in use.
		DHCPDECLINE(4),
		
		// Client broadcast to locate available servers.
		DHCPDISCOVER(1),
		
		// Client to server, asking only for local configuration
		// parameters; client already has externally configured
		// network address.
		DHCPINFORM(8),
		
		// Server to client indicating client's notion of network
		// address is incorrect (e.g., client has moved to new
		// subnet) or client's lease as expired
		DHCPNAK(6),
		
		// Server to client in response to DHCPDISCOVER with
		// offer of configuration parameters.
		DHCPOFFER(2),
		
		// Client to server relinquishing network address and
		// canceling remaining lease.
		DHCPRELEASE(7),
		
		// Client message to servers either (a) requesting
		// offered parameters from one server and implicitly
		// declining offers from all others, (b) confirming
		// correctness of previously allocated address after,
		// e.g., system reboot, or (c) extending the lease on a
		// particular network address.
		DHCPREQUEST(3);
		
		/**
		 * Convert id.
		 *
		 * @param id the id
		 * @return the DHCP message type
		 */
		public static DHCPMessageType convertID(byte id) {
			DHCPMessageType[] type = DHCPMessageType.values();
			for (int i = 0; i < type.length; i++) {
				if (type[i].compare(id))
					return type[i];
			}
			return null;
		}

		// Note: Additional DHCP message types can be found in RFCs 3203, 4388,
		// 6926 and 7724
		private final byte id;

		/**
		 * Instantiates a new DHCP message type.
		 *
		 * @param id the id
		 */
		DHCPMessageType(int id) {
			this.id = (byte) id;
		}

		/**
		 * Compare.
		 *
		 * @param id the id
		 * @return true, if successful
		 */
		public boolean compare(byte id) {
			return this.id == id;
		}

		/**
		 * Gets the id.
		 *
		 * @return the id
		 */
		public byte getID() {
			return this.id;
		}
	}

	/**
	 * The Enum DHCPOptionType.
	 */
	public enum DHCPOptionType {
	    
    	BootFileSize(13, 2),
    	BroadcastAddress(28, 4),
    	ClassIdentifier(60, 1, true),
    	ClientIdentifier(61, 2, true),
    	CookieServer(8, 4, true),
    	DHCPMessage(53, 1),
    	DomainName(15, 1, true),
    	DomainNameServer(6, 4, true),
    	End(255, 0),
    	ExtensionsPath(18, 1, true),
    	Hostname(12, 1, true),
    	ImpressServer(10, 4, true),
    	IPAddressLeaseTime(51, 4),
    	LogServer(7, 4, true),
    	LPRServer(9, 4, true),
    	MaxDHCPMessageSize(57, 2),
    	MeritDumpFile(14, 1, true),
    	Message(56, 1, true),
    	NameServer(5, 4, true),
    	OptionOverload(52, 1),
    	Pad(0, 0),
    	ParameterRequestList(55, 1, true),
    	RebindingTime(59, 4),
    	RenewalTime(58, 4),
    	RequestedIPAddress(50, 4),
    	ResourceLocation(11, 4, true),
    	RootPath(17, 1, true),
    	Router(3, 4, true),
    	ServerIdentifier(54, 4),
    	SubnetMask(1, 4),
    	SwapServer(16, 4, true),
    	TimeOffset(2, 4),
    	TimeServer(4, 4, true),
		Undefined(254, 0, true);

		/**
		 * Convert id.
		 *
		 * @param id the id
		 * @return the DHCP option type
		 */
		public static DHCPOptionType convertID(byte id) {
			DHCPOptionType[] option = DHCPOptionType.values();
			for (int i = 0; i < option.length; i++) {
				if (option[i].compare(id))
					return option[i];
			}
			return DHCPOptionType.Undefined;
		}
		
		/** The id. */
		private final byte id;
		
		/** The length. */
		private byte length;

		/** The variable length. */
		private boolean variableLength = false;

		/**
		 * Instantiates a new DHCP option type.
		 *
		 * @param id the id
		 * @param length the length
		 */
		DHCPOptionType(int id, int length) {
			this.id = (byte) id;
			this.length = (byte) length;
		}

		/**
		 * Instantiates a new DHCP option type.
		 *
		 * @param id the id
		 * @param length the length
		 * @param variableLength the variable length
		 */
		DHCPOptionType(int id, int length, boolean variableLength) {
			this.id = (byte) id;
			this.length = (byte) length;
			this.variableLength = variableLength;
		}

		/**
		 * Compare.
		 *
		 * @param id the id
		 * @return true, if successful
		 */
		public boolean compare(byte id) {
			return this.id == id;
		}

		/**
		 * Gets the id.
		 *
		 * @return the id
		 */
		public byte getID() {
			return this.id;
		}

	}

	/** The data. */
	private byte[] data;

	/** The length. */
	private byte length;
	
	/** The type. */
	private DHCPOptionType type;
	
	/**
	 * Instantiates a new DHCP option.
	 *
	 * @param type the type
	 * @param length the length
	 * @param data the data
	 */
	DHCPOption(byte type, byte length, byte[] data) {
		this.type = DHCPOptionType.convertID(type);
		this.length = length;
		this.data = data;
	}

	/**
	 * Instantiates a new DHCP option.
	 *
	 * @param optionArray the option array
	 */
	DHCPOption(byte[] optionArray) {
		if (optionArray.length < 1) {
			throw new RuntimeException("wat");
		}
		this.type = DHCPOptionType.convertID(optionArray[0]);
		if (optionArray.length < 2) {
			this.length = 0;

		} else {
			this.length = optionArray[1];
		}
		this.data = new byte[this.length];
		for (int i = 0; i < this.length; i++) {
			this.data[i] = optionArray[i + 2];
		}
	}

	/**
	 * Instantiates a new DHCP option.
	 *
	 * @param option the option
	 */
	DHCPOption(DHCPOptionType option) {
		this.type = option;
		this.length = option.length;
		this.data = new byte[this.length];
	}

	/**
	 * Equals.
	 *
	 * @param other the other
	 * @return true, if successful
	 */
	public boolean equals(DHCPOption other) {
		return this.type.equals(other.getType());
	}

	/**
	 * Gets the data.
	 *
	 * @return the data
	 */
	public byte[] getData() {
		return this.data;
	}

	/**
	 * Gets the length.
	 *
	 * @return the length
	 */
	public byte getLength() {
		return this.length;
	}

	/**
	 * Gets the type.
	 *
	 * @return the type
	 */
	public DHCPOptionType getType() {
		return type;
	}

	/**
	 * Sets the data.
	 *
	 * @param data the new data
	 */
	public void setData(byte data) {
		if (this.length == 1) {
			this.data = new byte[] { data };
		} else {
			throw new RuntimeException("Need more data");
		}

	}

	/**
	 * Sets the data.
	 *
	 * @param data the new data
	 */
	public void setData(byte[] data) {
		if (data.length == this.length) {
			this.data = data;
		} else if (data.length <= this.length) {
			data = new byte[this.length];
			for (int i = 0; i < data.length; i++) {
				this.data[i] = data[i];
			}
		} else if (this.type.variableLength) {
			setLength((byte) data.length);
			this.setData(data);
		} else {
			throw new RuntimeException("Optiondata too long");
		}

	}

	/**
	 * Sets the data.
	 *
	 * @param data the new data
	 */
	public void setData(int data) {
		setData(DHCPPacket.intToBytes(data));
	}

	/**
	 * Sets the length.
	 *
	 * @param length the new length
	 */
	public void setLength(byte length) {
		if (this.type.variableLength && length >= this.type.length) {
			this.length = length;
		} else if (length < this.length) {
			throw new RuntimeException("wrong option length");
		}
	}
	
	/* 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		if (this.type == DHCPOptionType.SubnetMask || this.type == DHCPOptionType.ServerIdentifier
				|| this.type == DHCPOptionType.RequestedIPAddress) {
			return this.type + ": " + DHCPPacket.ipByteToHex(this.data);
		} else if (this.type == DHCPOptionType.TimeOffset || this.type == DHCPOptionType.TimeServer
				|| this.type == DHCPOptionType.IPAddressLeaseTime || this.type == DHCPOptionType.RebindingTime
				|| this.type == DHCPOptionType.RenewalTime) {
			return this.type + ": " + DHCPPacket.byteToTime(this.data);
		} else if (this.type == DHCPOptionType.DHCPMessage) {
			return this.type + ": " + DHCPMessageType.convertID(this.data[0]);
		} else if (this.type == DHCPOptionType.Message) {
			return this.type + ": " + DHCPPacket.byteToString(this.data);
		} else {
			return this.type + ": 0x" + DHCPPacket.byteToHex(this.data);
		}
	}
}
