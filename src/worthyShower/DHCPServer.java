package worthyShower;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.SocketException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import worthyShower.DHCPOption.DHCPMessageType;

/**
 * The Class DHCPServer.
 */
public class DHCPServer implements Runnable {
	
	/**
	 * Server print.
	 *
	 * @param a the a
	 */
	private static void serverPrint(Object a) {
		System.out.println(DHCPServer.tab + "Server: " + a.toString());
	}
	
	/** verbose toggle */
	private static boolean verbose = false;
	
	/** The indentation */
	private static String tab = "                                                            ";
	
	/** The conv ip. */
	private InetAddress convIP;
	
	/** The conv port. */
	private int convPort;
	
	/** The ip pool. */
	private List<Integer> ipPool = new ArrayList<Integer>();

	/** The lease duration. */
	private long leaseDuration = 60000;

	/** The leases. */
	private HashMap<Long, Lease> leases = new HashMap<Long, Lease>();

	/** The socket. */
	private final DatagramSocket socket;

	/**
	 * Instantiates a new DHCP server.
	 *
	 * @param socket the socket
	 */
	DHCPServer(DatagramSocket socket) {
		serverPrint("server thread started!");
		this.socket = socket;
		try {
			this.socket.setSoTimeout(3000);
		} catch (SocketException e) {
			e.printStackTrace();
		}
		int ipRangeStart = 0xC0A81402;
		int ipRangeStop = 0xC0A814FE;
		for (int i = ipRangeStart; ipRangeStop >= i; i++) {
			this.ipPool.add(i);
		}
	}

	/**
	 * Activate lease.
	 *
	 * @param mac the mac
	 */
	public void activateLease(long mac) {
		Lease a = this.leases.get(mac);
		a.setActive(true);
		a.renew();
		this.ipPool.remove((Object) a.getIp());
	}

	/**
	 * Adds the lease.
	 *
	 * @param lease the lease
	 */
	public void addLease(Lease lease) {
		this.leases.put(lease.getMac(), lease);
	}

	/**
	 * Deactivate lease.
	 *
	 * @param mac the mac
	 */
	public void deactivateLease(long mac) {
		Lease a = this.leases.get(mac);
		if (a.isActive()) {
			a.setActive(false);
			this.ipPool.add(a.getIp());
		}
		;

	}

	/**
	 * Gets the conv ip.
	 *
	 * @return the conv ip
	 */
	public InetAddress getConvIP() {
		return convIP;
	}

	/**
	 * Gets the conv port.
	 *
	 * @return the conv port
	 */
	public int getConvPort() {
		return convPort;
	}

	/**
	 * Gets the lease.
	 *
	 * @param mac the mac
	 * @return the lease
	 */
	public Lease getLease(long mac) {
		return leases.get(mac);
	}

	/**
	 * Handle discover.
	 *
	 * @param requestPacket the request packet
	 * @return the DHCP packet
	 */
	private DHCPPacket handleDiscover(DHCPPacket requestPacket) {
		long mac = DHCPPacket.byteToLong(requestPacket.getChaddr());
		Lease newLease = makeLease(mac);
		if (newLease == null)
			return null;

		requestPacket.setOp(DHCPPacket.OpcodeType.BOOTREPLY);
		requestPacket.setHtype(DHCPPacket.HWAddressType.Ethernet);
		requestPacket.setHlen(6);
		requestPacket.setHops(0);
		// xid already set
		requestPacket.setSecs(0);
		// flags already set
		requestPacket.setCiaddr(0);
		requestPacket.setYiaddr(newLease.getIp());
		requestPacket.setSiaddr(0);
		// giaddr already set
		// chaddr already set
		requestPacket.setSname(new byte[64]);
		requestPacket.setFile(new byte[128]);
		requestPacket.resetOptions();

		DHCPOption leaseTime = new DHCPOption(DHCPOption.DHCPOptionType.IPAddressLeaseTime);
		leaseTime.setData(DHCPPacket.intToBytes((int) this.leaseDuration / 1000));
		requestPacket.addOption(leaseTime);

		DHCPOption message = new DHCPOption(DHCPOption.DHCPOptionType.Message);
		message.setData("yolo".getBytes());
		requestPacket.addOption(message);

		DHCPOption serverIdentifier = new DHCPOption(DHCPOption.DHCPOptionType.ServerIdentifier);
		serverIdentifier.setData(this.getConvIP().getAddress());
		requestPacket.addOption(serverIdentifier);

		DHCPOption reqOption = new DHCPOption(DHCPOption.DHCPOptionType.DHCPMessage);
		reqOption.setData(DHCPOption.DHCPMessageType.DHCPOFFER.getID());
		requestPacket.addOption(reqOption);
		return requestPacket;
	}

	/**
	 * Handle release.
	 *
	 * @param receivedPacket the received packet
	 */
	private void handleRelease(DHCPPacket receivedPacket) {
		long mac = DHCPPacket.byteToLong(receivedPacket.getChaddr());
		deactivateLease(mac);
		printLeases();
	}
	
	/**
	 * Handle request.
	 *
	 * @param receivedPacket the received packet
	 * @return the DHCP packet
	 */
	private DHCPPacket handleRequest(DHCPPacket receivedPacket) {
		long mac = DHCPPacket.byteToLong(receivedPacket.getChaddr());
		if (getLease(mac) == null) {
			return makeNak(receivedPacket);
		}
		activateLease(mac);
		printLeases();
		return makeAck(receivedPacket);
	}

	/**
	 * Make ack.
	 *
	 * @param requestPacket the request packet
	 * @return the DHCP packet
	 */
	private DHCPPacket makeAck(DHCPPacket requestPacket) {
		long mac = DHCPPacket.byteToLong(requestPacket.getChaddr());
		Lease lease = getLease(mac);
		requestPacket.setOp(DHCPPacket.OpcodeType.BOOTREPLY);
		requestPacket.setHtype(DHCPPacket.HWAddressType.Ethernet);
		requestPacket.setHlen(6);
		requestPacket.setHops(0);
		// xid already set
		requestPacket.setSecs(0);
		// flags already set
		// ciaddr already set
		requestPacket.setYiaddr(lease.getIp());
		requestPacket.setSiaddr(0);
		// giaddr already set
		// chaddr already set
		requestPacket.setSname(new byte[64]);
		requestPacket.setFile(new byte[128]);
		requestPacket.resetOptions();

		if (requestPacket.getDHCPType() == DHCPMessageType.DHCPREQUEST) {
			DHCPOption leaseTime = new DHCPOption(DHCPOption.DHCPOptionType.IPAddressLeaseTime);
			leaseTime.setData(DHCPPacket.intToBytes((int) this.leaseDuration / 1000));
			requestPacket.addOption(leaseTime);
		}

		DHCPOption message = new DHCPOption(DHCPOption.DHCPOptionType.Message);
		message.setData("yolo".getBytes());
		requestPacket.addOption(message);

		DHCPOption serverIdentifier = new DHCPOption(DHCPOption.DHCPOptionType.ServerIdentifier);
		serverIdentifier.setData(this.getConvIP().getAddress());
		requestPacket.addOption(serverIdentifier);

		DHCPOption reqOption = new DHCPOption(DHCPOption.DHCPOptionType.DHCPMessage);
		reqOption.setData(DHCPOption.DHCPMessageType.DHCPACK.getID());
		requestPacket.addOption(reqOption);
		return requestPacket;
	}

	/**
	 * Make lease.
	 *
	 * @param mac the mac
	 * @return the lease
	 */
	public Lease makeLease(byte[] mac) {
		return makeLease(DHCPPacket.byteToLong(mac));
	}

	/**
	 * Make lease.
	 *
	 * @param mac the mac
	 * @return the lease
	 */
	public Lease makeLease(long mac) {
		if (this.ipPool.isEmpty()) {
			return null;
		}
		if (this.leases.containsKey(mac)) {
			return this.leases.get(mac);
		}
		int ip = this.ipPool.get(0);
		this.ipPool.remove(0);
		this.ipPool.add(ip);
		Lease newLease = new Lease(mac, ip, System.currentTimeMillis(), this.leaseDuration);
		this.addLease(newLease);
		return newLease;

	}
	
	/**
	 * Make nak.
	 *
	 * @param requestPacket the request packet
	 * @return the DHCP packet
	 */
	private DHCPPacket makeNak(DHCPPacket requestPacket) {
		requestPacket.setOp(DHCPPacket.OpcodeType.BOOTREPLY);
		requestPacket.setHtype(DHCPPacket.HWAddressType.Ethernet);
		requestPacket.setHlen(6);
		requestPacket.setHops(0);
		// xid already set
		requestPacket.setSecs(0);
		// flags already set
		// ciaddr already set
		requestPacket.setYiaddr(0);
		requestPacket.setSiaddr(0);
		// giaddr already set
		// chaddr already setthis.leaseDuration
		requestPacket.setSname(new byte[64]);
		requestPacket.setFile(new byte[128]);
		requestPacket.resetOptions();

		DHCPOption message = new DHCPOption(DHCPOption.DHCPOptionType.Message);
		message.setData("yolo".getBytes());
		requestPacket.addOption(message);

		DHCPOption serverIdentifier = new DHCPOption(DHCPOption.DHCPOptionType.ServerIdentifier);
		serverIdentifier.setData(this.getConvIP().getAddress());
		requestPacket.addOption(serverIdentifier);

		DHCPOption reqOption = new DHCPOption(DHCPOption.DHCPOptionType.DHCPMessage);
		reqOption.setData(DHCPOption.DHCPMessageType.DHCPNAK.getID());
		requestPacket.addOption(reqOption);
		return requestPacket;
	}

	/**
	 * Prints the leases.
	 */
	public void printLeases() {
		StringBuilder sb = new StringBuilder();
		sb.append("+--------------------+-----------------+-----------------+\n");
		sb.append("|  Hardware address  |   IP address    | Lease time left |\n");
		sb.append("+--------------------+-----------------+-----------------+\n");
		this.leases.forEach((mac, lease) -> {
			if (lease.isActive()){
				sb.append("|                    |                 |                 |\n");
				String a = DHCPPacket.longToMac(lease.getMac()).substring(0, 17);
				sb.replace(sb.length()-57, sb.length()-57+a.length(), a);
				a =  DHCPPacket.intToIPString(lease.getIp());
				sb.replace(sb.length()-36, sb.length()-36+a.length(), a);
				a =  lease.getLeaseTimeLeft()/1000 + "s";
				sb.replace(sb.length()-18, sb.length()-18 + a.length(), a);
			}
		});
		sb.append("+--------------------+-----------------+-----------------+\n");
		sb.append("|                                                        |\n");
		String a = this.ipPool.size() + " of 253 IP addresses left";
		sb.replace(sb.length()-57, sb.length()-57+a.length(), a);
		sb.append("+--------------------------------------------------------+\n\n");
		System.out.print(sb.toString());
	}
	
	/**
	 * Receive packet.
	 *
	 * @return the byte[]
	 * @throws IOException Signals that an I/O exception has occurred.
	 */
	public byte[] receivePacket() throws IOException {
		byte[] receiveData = new byte[1024];
		DatagramPacket receivePacket = new DatagramPacket(receiveData, receiveData.length);
		this.socket.receive(receivePacket);
		this.setConvIP(receivePacket.getAddress());
		this.setConvPort(receivePacket.getPort());
		return receiveData;
	}

	/**
	 * Revoke leases.
	 */
	public void revokeLeases() {
		for (long key : this.leases.keySet()) {
			Lease a = this.leases.get(key);
			if (a.getLeaseTimeLeft() < 0) {
				deactivateLease(a.getMac());
			}
		}
	}

	/* 
	 * @see java.lang.Runnable#run()
	 */
	@Override
	public void run() {
		while (true) {
			byte[] data;
			try {
				data = receivePacket();
			} catch (IOException e) {
				revokeLeases();
				printLeases();
				continue;
			}
			DHCPPacket receivedPacket = new DHCPPacket(data);
			serverPrint("received " + receivedPacket.getDHCPType());
			if (verbose) System.out.println(receivedPacket.toTabbedString(DHCPServer.tab));
			if (receivedPacket.getDHCPType() == DHCPOption.DHCPMessageType.DHCPDISCOVER) {
				DHCPPacket sendPacket = handleDiscover(receivedPacket);
				if (sendPacket != null)
					this.sendReplyPacket(sendPacket);
			} else if (receivedPacket.getDHCPType() == DHCPOption.DHCPMessageType.DHCPREQUEST) {
				DHCPPacket sendPacket = handleRequest(receivedPacket);
				this.sendReplyPacket(sendPacket);
			} else if (receivedPacket.getDHCPType() == DHCPOption.DHCPMessageType.DHCPRELEASE) {
				handleRelease(receivedPacket);
		}
		}
	}

	/**
	 * Send reply packet.
	 *
	 * @param sendData the send data
	 */
	public void sendReplyPacket(byte[] sendData) {
		DatagramPacket sendPacket = new DatagramPacket(sendData, sendData.length, this.getConvIP(), this.getConvPort());
		try {
			this.socket.send(sendPacket);
		} catch (IOException e) {
			e.printStackTrace();
		}

	}

	/**
	 * Send reply packet.
	 *
	 * @param packet the packet
	 */
	public void sendReplyPacket(DHCPPacket packet) {

		serverPrint("sent " + packet.getDHCPType());
		if (verbose) System.out.println(packet.toTabbedString(DHCPServer.tab));
		this.sendReplyPacket(packet.toBytes());
	}

	/**
	 * Sets the conv ip.
	 *
	 * @param convIP the new conv ip
	 */
	public void setConvIP(InetAddress convIP) {
		this.convIP = convIP;
	}

	/**
	 * Sets the conv port.
	 *
	 * @param convPort the new conv port
	 */
	public void setConvPort(int convPort) {
		this.convPort = convPort;
	}
}
