package worthyShower;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.SocketException;

import worthyShower.DHCPOption.DHCPMessageType;
import worthyShower.DHCPOption.DHCPOptionType;

/**
 * The Class DHCPClient.
 */
public class DHCPClient implements Runnable {
	
	/**
	 * The Enum ClientState.
	 */
	public enum ClientState {

		BOUND, DYING, INIT, INITREBOOT, REBINDING, REBOOTING, RENEWING, REQUESTING, SELECTING, STALLING;
	}

	private static boolean verbose = false;
	
	/** The client ip. */
	private byte[] clientIP;
	
	/** The client socket. */
	private DatagramSocket clientSocket;
	
	/** The connected. */
	private boolean connected;
	
	/** The latest offer. */
	private DHCPPacket latestOffer;
	
	/** The mac. */
	private byte[] mac;
	
	/** The port. */
	private int port;
	
	/** The prev state. */
	private ClientState prevState;
	
	/** The server ip. */
	private InetAddress serverIP;
	
	/** The state. */
	private ClientState state;
	
	private static String tab = "                                                                                                    ";
	
	/** The timer one. */
	private long timerOne;

	/** The timer two. */
	private long timerTwo;

	/**
	 * Instantiates a new DHCP client.
	 *
	 * @param serverIP the server ip
	 * @param port the port
	 * @param mac the mac
	 */
	DHCPClient(InetAddress serverIP, int port, byte[] mac) {
		this.serverIP = serverIP;
		this.port = port;
		this.mac = mac;
		this.state = ClientState.INIT;
	}

	/**
	 * Client print.
	 *
	 * @param a the a
	 */
	private void clientPrint(Object a) {
		System.out.println(DHCPClient.tab + "Client"+ (this.mac[0] & 0xFF )+ ": " + a.toString());
	}

	/**
	 * End connection.
	 */
	public void endConnection() {
		this.clientSocket.close();
	}

	/**
	 * Gets the client ip.
	 *
	 * @return the client ip
	 */
	public byte[] getClientIP() {
		return clientIP;
	}

	/**
	 * Gets the latest offer.
	 *
	 * @return the latest offer
	 */
	public DHCPPacket getLatestOffer() {
		return latestOffer;
	}

	/**
	 * Gets the prev state.
	 *
	 * @return the prev state
	 */
	public ClientState getPrevState() {
		return prevState;
	}

	/**
	 * Gets the server ip.
	 *
	 * @return the server ip
	 */
	public InetAddress getServerIP() {
		return serverIP;
	}

	/**
	 * Gets the state.
	 *
	 * @return the state
	 */
	public ClientState getState() {
		return state;
	}

	/**
	 * Gets the timer one.
	 *
	 * @return the timer one
	 */
	public long getTimerOne() {
		return timerOne;
	}

	/**
	 * Gets the timer two.
	 *
	 * @return the timer two
	 */
	public long getTimerTwo() {
		return timerTwo;
	}

	/**
	 * Checks if is connected.
	 *
	 * @return true, if is connected
	 */
	public boolean isConnected() {
		return connected;
	}

	/**
	 * Make dhcp decline.
	 *
	 * @return the DHCP packet
	 */
	public DHCPPacket makeDHCPDecline() {
		DHCPPacket result = new DHCPPacket();
		result.setOp(DHCPPacket.OpcodeType.BOOTREQUEST);
		result.setHtype(DHCPPacket.HWAddressType.Ethernet);
		result.setHlen(6);
		result.setHops(0);
		// xid already set
		result.setSecs(0);
		result.setFlags(0x0000);
		result.setCiaddr(0);
		result.setYiaddr(0);
		result.setSiaddr(0);
		result.setGiaddr(0);
		result.setChaddr(this.mac);
		result.setSname(new byte[64]);
		result.setFile(new byte[128]);

		DHCPOption dhcpOption = new DHCPOption(DHCPOption.DHCPOptionType.DHCPMessage);
		dhcpOption.setData(DHCPOption.DHCPMessageType.DHCPDECLINE.getID());
		result.addOption(dhcpOption);

		DHCPOption ipRequest = new DHCPOption(DHCPOption.DHCPOptionType.RequestedIPAddress);
		ipRequest.setData(this.getLatestOffer().getYiaddr());
		result.addOption(ipRequest);

		DHCPOption serverIdentifier = new DHCPOption(DHCPOption.DHCPOptionType.ServerIdentifier);
		serverIdentifier.setData(this.getServerIP().getAddress());
		result.addOption(serverIdentifier);

		return result;
	}

	/**
	 * Make dhcp discover.
	 *
	 * @return the DHCP packet
	 */
	public DHCPPacket makeDHCPDiscover() {
		DHCPPacket result = new DHCPPacket();
		result.setOp(DHCPPacket.OpcodeType.BOOTREQUEST);
		result.setHtype(DHCPPacket.HWAddressType.Ethernet);
		result.setHlen(6);
		result.setHops(0);
		// xid already set
		result.setSecs(0);
		result.setFlags(0x0000);
		result.setCiaddr(0);
		result.setYiaddr(0);
		result.setSiaddr(0);
		result.setGiaddr(0);
		result.setChaddr(this.mac);
		result.setSname(new byte[64]);
		result.setFile(new byte[128]);
		DHCPOption dhcpOption = new DHCPOption(DHCPOption.DHCPOptionType.DHCPMessage);
		dhcpOption.setData(DHCPOption.DHCPMessageType.DHCPDISCOVER.getID());
		result.addOption(dhcpOption);

		return result;
	}

	/**
	 * Make dhcp release.
	 *
	 * @return the DHCP packet
	 */
	public DHCPPacket makeDHCPRelease() {
		DHCPPacket result = new DHCPPacket();
		result.setOp(DHCPPacket.OpcodeType.BOOTREQUEST);
		result.setHtype(DHCPPacket.HWAddressType.Ethernet);
		result.setHlen(6);
		result.setHops(0);
		// xid already set
		result.setSecs(0);
		result.setFlags(0x0000);
		result.setCiaddr(DHCPPacket.byteToInt(this.getClientIP()));
		result.setYiaddr(0);
		result.setSiaddr(0);
		result.setGiaddr(0);
		result.setChaddr(this.mac);
		result.setSname(new byte[64]);
		result.setFile(new byte[128]);

		DHCPOption dhcpOption = new DHCPOption(DHCPOption.DHCPOptionType.DHCPMessage);
		dhcpOption.setData(DHCPOption.DHCPMessageType.DHCPRELEASE.getID());
		result.addOption(dhcpOption);

		DHCPOption serverIdentifier = new DHCPOption(DHCPOption.DHCPOptionType.ServerIdentifier);
		serverIdentifier.setData(this.getServerIP().getAddress());
		result.addOption(serverIdentifier);

		return result;
	}

	/**
	 * Make dhcp request.
	 *
	 * @param offer the offer
	 * @return the DHCP packet
	 */
	public DHCPPacket makeDHCPRequest(DHCPPacket offer) {
		offer.setOp(DHCPPacket.OpcodeType.BOOTREQUEST);
		offer.setHtype(DHCPPacket.HWAddressType.Ethernet);
		offer.setHlen(6);
		offer.setHops(0);
		// xid already set
		offer.setSecs(0);
		offer.setFlags(0x0000);
		int givenIP = offer.getYiaddr();
		offer.setYiaddr(0);
		offer.setSiaddr(0);
		offer.setGiaddr(0);
		offer.setChaddr(this.mac);
		offer.setSname(new byte[64]);
		offer.setFile(new byte[128]);

		if (this.getState() == ClientState.SELECTING) {
			offer.setCiaddr(0);

			DHCPOption serverIdentifier = offer.getOption(DHCPOption.DHCPOptionType.ServerIdentifier.getID());

			offer.resetOptions();

			DHCPOption ipRequest = new DHCPOption(DHCPOption.DHCPOptionType.RequestedIPAddress);
			ipRequest.setData(givenIP);
			offer.addOption(ipRequest);

			offer.addOption(serverIdentifier);

		} else if (getState() == ClientState.INITREBOOT) {
			offer.setCiaddr(0);

			offer.resetOptions();

			DHCPOption ipRequest = new DHCPOption(DHCPOption.DHCPOptionType.RequestedIPAddress);
			ipRequest.setData(givenIP);
			offer.addOption(ipRequest);

		} else if ((getState() == ClientState.RENEWING) || (getState() == ClientState.REBINDING)
				|| (getState() == ClientState.BOUND)) {
			offer.setCiaddr(DHCPPacket.byteToInt(this.getClientIP()));
			offer.resetOptions();
		} else {
			throw new RuntimeException("wrong time, wrong state");
		}

		DHCPOption dhcpOption = new DHCPOption(DHCPOption.DHCPOptionType.DHCPMessage);
		dhcpOption.setData(DHCPOption.DHCPMessageType.DHCPREQUEST.getID());
		offer.addOption(dhcpOption);

		return offer;
	}

	/**
	 * Receive packet.
	 *
	 * @return the byte[]
	 * @throws IOException Signals that an I/O exception has occurred.
	 */
	public byte[] receivePacket() throws IOException {
		byte[] receiveData = new byte[1024];
		DatagramPacket receivePacket = new DatagramPacket(receiveData, receiveData.length);
		this.clientSocket.receive(receivePacket);
		if (verbose) System.out.println(new DHCPPacket(receiveData).toTabbedString(DHCPClient.tab));
		return receiveData;
	}

	/* 
	 * Fsm of the client.
	 */
	@Override
	public void run() {
		this.startConnection();
		while (true) {
			if (this.getPrevState() != this.getState())
				clientPrint("state: " + this.getState());
			this.setPrevState(this.getState());
			if (getState() == ClientState.INIT) {
				DHCPPacket inform = makeDHCPDiscover();
				sendPacket(inform);
				this.setState(ClientState.SELECTING);

			} else if (getState() == ClientState.SELECTING) {
				DHCPPacket offer = null;
				while (offer == null || offer.getDHCPType() != DHCPMessageType.DHCPOFFER) {
					try {
						byte[] receive;
						receive = receivePacket();
						offer = new DHCPPacket(receive);
					} catch (IOException e) {
						sendPacket(makeDHCPDiscover());
						continue;
					}
					clientPrint("received " + offer.getDHCPType());
				}
				this.setLatestOffer(offer);
				DHCPPacket request = this.makeDHCPRequest(offer);
				sendPacket(request);
				this.setState(ClientState.REQUESTING);

			} else if (getState() == ClientState.REQUESTING) {
				DHCPPacket answer = null;
				while (answer == null || (answer.getDHCPType() != DHCPMessageType.DHCPACK)
						&& (answer.getDHCPType() != DHCPMessageType.DHCPNAK)) {
					try {
						byte[] receive;
						receive = receivePacket();
						answer = new DHCPPacket(receive);
					} catch (IOException e) {
						continue;
					}
					clientPrint("received " + answer.getDHCPType());
				}
				if (answer.getDHCPType() == DHCPMessageType.DHCPACK) {
					int leaseTime = DHCPPacket
							.byteToInt(answer.getOption(DHCPOptionType.IPAddressLeaseTime.getID()).getData());
					this.setTimerOne(System.currentTimeMillis() + leaseTime * 500);
					this.setTimerTwo(System.currentTimeMillis() + leaseTime * 1000);
					this.setClientIP(DHCPPacket.intToBytes(answer.getYiaddr()));
					this.setState(ClientState.BOUND);
				} else {
					this.setState(ClientState.INIT);
				}

			} else if (getState() == ClientState.BOUND) {
				this.setConnected(true);
				if (System.currentTimeMillis() < this.getTimerOne()) {
					try {
						Thread.sleep(1000);
					} catch (InterruptedException e) {
					}
				} else {
					DHCPPacket request = this.makeDHCPRequest(this.getLatestOffer());
					sendPacket(request);
					this.setState(ClientState.RENEWING);
				}

			} else if (getState() == ClientState.RENEWING) {
				if (System.currentTimeMillis() > this.getTimerTwo()) {
					DHCPPacket request = this.makeDHCPRequest(this.getLatestOffer());
					sendPacket(request);
					this.setState(ClientState.REBINDING);
					continue;
				}
				DHCPPacket answer = null;
				try {
					byte[] receive;
					receive = receivePacket();
					answer = new DHCPPacket(receive);
				} catch (IOException e) {
					continue;
				}
				clientPrint("received " + answer.getDHCPType());
				if (answer.getDHCPType() == DHCPMessageType.DHCPACK) {
					int leaseTime = DHCPPacket
							.byteToInt(answer.getOption(DHCPOptionType.IPAddressLeaseTime.getID()).getData());
					this.setTimerOne(System.currentTimeMillis() + leaseTime * 500);
					this.setTimerTwo(System.currentTimeMillis() + leaseTime * 1000);
					this.setClientIP(DHCPPacket.intToBytes(answer.getYiaddr()));
					this.setState(ClientState.BOUND);
				} else if (answer.getDHCPType() == DHCPMessageType.DHCPNAK) {
					this.setConnected(false);
					this.setState(ClientState.INIT);
				}

			} else if (getState() == ClientState.REBINDING) {
				DHCPPacket answer = null;
				try {
					byte[] receive;
					receive = receivePacket();
					answer = new DHCPPacket(receive);
				} catch (IOException e) {
					this.setConnected(false);
					this.setState(ClientState.INIT);
				}
				clientPrint("received " + answer.getDHCPType());
				if (answer.getDHCPType() == DHCPMessageType.DHCPACK) {
					int leaseTime = DHCPPacket
							.byteToInt(answer.getOption(DHCPOptionType.IPAddressLeaseTime.getID()).getData());
					this.setTimerOne(System.currentTimeMillis() + leaseTime * 500);
					this.setTimerTwo(System.currentTimeMillis() + leaseTime * 1000);
					this.setClientIP(DHCPPacket.intToBytes(answer.getYiaddr()));
					this.setState(ClientState.BOUND);
				} else if (answer.getDHCPType() == DHCPMessageType.DHCPNAK) {
					this.setConnected(false);
					this.setState(ClientState.INIT);
				}

			} else if (getState() == ClientState.DYING) {
				DHCPPacket release = this.makeDHCPRelease();
				sendPacket(release);
				this.setConnected(false);
				this.setState(ClientState.STALLING);
				
			} else if (getState() == ClientState.STALLING) {
				try {
					Thread.sleep(10000);
				} catch (InterruptedException e) {
				}

			} else if (getState() == ClientState.INITREBOOT) {
				DHCPPacket request = this.makeDHCPRequest(this.getLatestOffer());
				sendPacket(request);
				this.setState(ClientState.REBOOTING);

			} else if (getState() == ClientState.REBOOTING) {
				DHCPPacket answer = null;
				try {
					byte[] receive;
					receive = receivePacket();
					answer = new DHCPPacket(receive);
				} catch (IOException e) {
					this.setState(ClientState.INIT);
				}
				clientPrint("received " + answer.getDHCPType());
				if (answer.getDHCPType() == DHCPMessageType.DHCPACK) {
					int leaseTime = DHCPPacket
							.byteToInt(answer.getOption(DHCPOptionType.IPAddressLeaseTime.getID()).getData());
					this.setTimerOne(System.currentTimeMillis() + leaseTime * 500);
					this.setTimerTwo(System.currentTimeMillis() + leaseTime * 1000);
					this.setClientIP(DHCPPacket.intToBytes(answer.getYiaddr()));
					this.setConnected(true);
					this.setState(ClientState.BOUND);
				} else if (answer.getDHCPType() == DHCPMessageType.DHCPNAK) {
					this.setConnected(false);
					this.setState(ClientState.INIT);
				}
			}
		}

	}

	/**
	 * Send packet.
	 *
	 * @param sendData the send data
	 */
	public void sendPacket(byte[] sendData) {
		DatagramPacket sendPacket = new DatagramPacket(sendData, sendData.length, this.getServerIP(), this.port);
		try {
			this.clientSocket.send(sendPacket);
		} catch (IOException e) {
			e.printStackTrace();
		}

	}

	/**
	 * Send packet.
	 *
	 * @param packet the packet
	 */
	public void sendPacket(DHCPPacket packet) {
		clientPrint("sent " + packet.getDHCPType());
		if (verbose) System.out.println(packet.toTabbedString(DHCPClient.tab));
		this.sendPacket(packet.toBytes());
	}

	/**
	 * Sets the client ip.
	 *
	 * @param clientIP the new client ip
	 */
	public void setClientIP(byte[] clientIP) {
		if (clientIP.length == 4)
			this.clientIP = clientIP;
	}

	/**
	 * Sets the connected.
	 *
	 * @param connected the new connected
	 */
	public void setConnected(boolean connected) {
		this.connected = connected;
	}

	/**
	 * Sets the latest offer.
	 *
	 * @param latestOffer the new latest offer
	 */
	public void setLatestOffer(DHCPPacket latestOffer) {
		this.latestOffer = latestOffer;
	}

	/**
	 * Sets the prev state.
	 *
	 * @param state the new prev state
	 */
	public void setPrevState(ClientState state) {
		this.prevState = state;
	}

	/**
	 * Sets the server ip.
	 *
	 * @param serverIP the new server ip
	 */
	public void setServerIP(InetAddress serverIP) {
		this.serverIP = serverIP;
	}

	/**
	 * Sets the state.
	 *
	 * @param state the new state
	 */
	public void setState(ClientState state) {
		this.state = state;
	}

	/**
	 * Sets the timer one.
	 *
	 * @param timerOne the new timer one
	 */
	public void setTimerOne(long timerOne) {
		this.timerOne = timerOne;
	}

	/**
	 * Sets the timer two.
	 *
	 * @param timerTwo the new timer two
	 */
	public void setTimerTwo(long timerTwo) {
		this.timerTwo = timerTwo;
	}

	/**
	 * Start connection.
	 */
	public void startConnection() {

		try {
			this.clientSocket = new DatagramSocket();
			this.clientSocket.setSoTimeout(3000);
		} catch (SocketException e) {
			e.printStackTrace();
		}

	}
	
	/*
	
 --------                               -------
|        | +-------------------------->|       |<-------------------+
| INIT-  | |     +-------------------->| INIT  |                    |
| REBOOT |DHCPNAK/         +---------->|       |<---+               |
|        |Restart|         |            -------     |               |
 --------  |  DHCPNAK/     |               |                        |
    |      Discard offer   |      -/Send DHCPDISCOVER               |
-/Send DHCPREQUEST         |               |                        |
    |      |     |      DHCPACK            v        |               |
 -----------     |   (not accept.)/   -----------   |               |
|           |    |  Send DHCPDECLINE |           |                  |
| REBOOTING |    |         |         | SELECTING |<----+            |
|           |    |        /          |           |     |DHCPOFFER/  |
 -----------     |       /            -----------   |  |Collect     |
    |            |      /                  |   |       |  replies   |
DHCPACK/         |     /  +----------------+   +-------+            |
Record lease, set|    |   v   Select offer/                         |
timers T1, T2   ------------  send DHCPREQUEST      |               |
    |   +----->|            |             DHCPNAK, Lease expired/   |
    |   |      | REQUESTING |                  Halt network         |
    DHCPOFFER/ |            |                       |               |
    Discard     ------------                        |               |
    |   |        |        |                   -----------           |
    |   +--------+     DHCPACK/              |           |          |
    |              Record lease, set    -----| REBINDING |          |
    |                timers T1, T2     /     |           |          |
    |                     |        DHCPACK/   -----------           |
    |                     v     Record lease, set   ^               |
    +----------------> -------      /timers T1,T2   |               |
               +----->|       |<---+                |               |
               |      | BOUND |<---+                |               |
  DHCPOFFER, DHCPACK, |       |    |            T2 expires/   DHCPNAK/
   DHCPNAK/Discard     -------     |             Broadcast  Halt network
               |       | |         |            DHCPREQUEST         |
               +-------+ |        DHCPACK/          |               |
                    T1 expires/   Record lease, set |               |
                 Send DHCPREQUEST timers T1, T2     |               |
                 to leasing server |                |               |
                         |   ----------             |               |
                         |  |          |------------+               |
                         +->| RENEWING |                            |
                            |          |----------------------------+
                             ----------
	 */
}
