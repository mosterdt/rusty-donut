package worthyShower;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.util.HashMap;
import java.util.Random;
import java.util.stream.Collectors;

import worthyShower.DHCPOption.DHCPMessageType;
import worthyShower.DHCPOption.DHCPOptionType;

/**
 * The Class DHCPPacket.
 */
public class DHCPPacket {

	/**
	 * The Enum HWAddressType.
	 */
	public enum HWAddressType {
		
		/** The Ethernet. */
		Ethernet(1);
		
		/**
		 * Convert id.
		 *
		 * @param id the id
		 * @return the HW address type
		 */
		public static HWAddressType convertID(byte id) {
			HWAddressType[] type = HWAddressType.values();
			for (int i = 0; i < type.length; i++) {
				if (type[i].compare(id))
					return type[i];
			}
			return null;
		}

		/** The id. */
		private final byte id;

		/**
		 * Instantiates a new HW address type.
		 *
		 * @param id the id
		 */
		HWAddressType(int id) {
			this.id = (byte) id;
		}

		/**
		 * Compare.
		 *
		 * @param id the id
		 * @return true, if successful
		 */
		public boolean compare(byte id) {
			return this.id == id;
		}
	}

	/**
	 * The Enum OpcodeType.
	 */
	public enum OpcodeType {
		
		BOOTREPLY(2),
		BOOTREQUEST(1);
		
		/**
		 * Convert id.
		 *
		 * @param id the id
		 * @return the opcode type
		 */
		public static OpcodeType convertID(byte id) {
			OpcodeType[] type = OpcodeType.values();
			for (int i = 0; i < type.length; i++) {
				if (type[i].compare(id))
					return type[i];
			}
			return null;
		}

		/** The id. */
		private final byte id;

		/**
		 * Instantiates a new opcode type.
		 *
		 * @param id the id
		 */
		OpcodeType(int id) {
			this.id = (byte) id;
		}

		/**
		 * Compare.
		 *
		 * @param id the id
		 * @return true, if successful
		 */
		public boolean compare(byte id) {
			return this.id == id;
		}
	}

	/** The Constant hexArray. */
	final protected static char[] hexArray = "0123456789ABCDEF".toCharArray();
	
	/** The magic cookie. */
	private static byte[] magicCookie = new byte[] { (byte) 0x63, (byte) 0x82, (byte) 0x53, (byte) 0x63 };
	
	/**
	 * Byte to hex.
	 *
	 * @param bytes the bytes
	 * @return the string
	 */
	public static String byteToHex(byte[] bytes) {
		char[] hexChars = new char[bytes.length * 2];
		for (int j = 0; j < bytes.length; j++) {
			int v = bytes[j] & 0xFF;
			hexChars[j * 2] = hexArray[v >>> 4];
			hexChars[j * 2 + 1] = hexArray[v & 0x0F];
		}
		return new String(hexChars);
	}
	
	/**
	 * Byte to int.
	 *
	 * @param i the i
	 * @return the int
	 */
	public static int byteToInt(byte[] i) {
		return (i[0] << 24) + (i[1] << 16) + (i[2] << 8) + i[3];
	}
	
	/**
	 * Byte to long.
	 *
	 * @param i the i
	 * @return the long
	 */
	public static long byteToLong(byte[] i) {
		long result = 0;
		for (int j = 0; j < 8; j++) {
			result = (result << 8) + (i[j] & 0xff);
			// result += (long) ((i[j] & 0xffL) << (6 * (5-j)));
		}
		return result;
	}
	
	/**
	 * Byte to mac.
	 *
	 * @param mac the mac
	 * @return the string
	 */
	public static String byteToMac(byte[] mac) {
		char[] hexChars = new char[mac.length * 3 - 1];
		for (int j = 0; j < mac.length; j++) {
			int v = mac[j] & 0xFF;
			hexChars[j * 3] = hexArray[v >>> 4];
			hexChars[j * 3 + 1] = hexArray[v & 0x0F];
			if (!(j == mac.length - 1))
				hexChars[j * 3 + 2] = ':';
		}
		return new String(hexChars);
	}
	
	/**
	 * Byte to string.
	 *
	 * @param data the data
	 * @return the string
	 */
	public static String byteToString(byte[] data) {
		if (data.length == 0)
			return "";
		String result = "";
		for (byte i : data) {
			if ((i & 0xFF) == 0)
				break;
			result += (char) i;
		}
		return result;
	}
	
	/**
	 * Byte to time.
	 *
	 * @param data the data
	 * @return the string
	 */
	public static String byteToTime(byte[] data) {
		String result = "";
		int time = data[3] & 0xFF | (data[2] & 0xFF) << 8 | (data[1] & 0xFF) << 16 | (data[0] & 0xFF) << 24;
		result += "(" + time + "s) ";
		if (time >= 3600) {
			result += time / 3600 + " hours, ";
			time = time % 3600;
		}
		if (time >= 60) {
			result += time / 60 + " minutes, ";
		}
		return result + time % 60 + " seconds";
	}
	
	/**
	 * Int to bytes.
	 *
	 * @param i the i
	 * @return the byte[]
	 */
	public static byte[] intToBytes(int i) {
		byte[] result = new byte[4];

		result[0] = (byte) (i >> 24);
		result[1] = (byte) (i >> 16);
		result[2] = (byte) (i >> 8);
		result[3] = (byte) (i /* >> 0 */);

		return result;
	}
	
	/**
	 * Int to ip string.
	 *
	 * @param ip the ip
	 * @return the string
	 */
	public static String intToIPString(int ip) {
		StringBuilder sb = new StringBuilder();
		sb.append((byte) (ip >>> 24) & 0xFF).append(".").append((byte) (ip >>> 16) & 0xFF).append(".")
				.append((byte) (ip >>> 8) & 0xFF).append(".").append((byte) ip & 0xFF);
		return sb.toString();
	}
	
	/**
	 * Ip byte to hex.
	 *
	 * @param ip the ip
	 * @return the string
	 */
	public static String ipByteToHex(byte[] ip) {
		StringBuilder sb = new StringBuilder();
		sb.append(ip[0] & 0xFF).append(".").append(ip[1] & 0xFF).append(".").append(ip[2] & 0xFF).append(".")
				.append(ip[3] & 0xFF);
		return sb.toString();
	}
	
	/**
	 * Long to bytes.
	 *
	 * @param l the l
	 * @return the byte[]
	 */
	public static byte[] longToBytes(long l) {
		byte[] result = new byte[8];
		for (int i = 0; i < result.length; i++) {
			result[i] = (byte) (l >> (8 * (7 - i)));
		}
		return result;
	}
	
	/**
	 * Long to mac.
	 *
	 * @param mac the mac
	 * @return the string
	 */
	public static String longToMac(long mac) {
		return byteToMac(longToBytes(mac));
	}
	
	/** The chaddr. */
	private byte[] chaddr = new byte[16];
	
	/** The ciaddr. */
	private int ciaddr;
	
	/** The dhcp type. */
	private DHCPMessageType dhcpType;
	
	/** The file. */
	private byte[] file = new byte[128];
	
	/** The flags. */
	private short flags;

	/** The giaddr. */
	private int giaddr;

	/** The hlen. */
	private byte hlen;

	/** The hops. */
	private byte hops;

	/** The htype. */
	private HWAddressType htype;

	/** The op. */
	private OpcodeType op;

	/** The options. */
	private HashMap<Byte, DHCPOption> options = new HashMap<Byte, DHCPOption>();

	/** The rand. */
	private Random rand = new Random();

	/** The secs. */
	private short secs;

	/** The siaddr. */
	private int siaddr;

	/** The sname. */
	private byte[] sname = new byte[64];

	/** The xid. */
	private int xid;

	/** The yiaddr. */
	private int yiaddr;

	/**
	 * Instantiates a new DHCP packet.
	 */
	DHCPPacket() {
		this.op = OpcodeType.BOOTREQUEST;
		this.htype = HWAddressType.Ethernet;
		this.hlen = 6;
		this.hops = 0;
		this.xid = rand.nextInt();
		this.secs = 0;
		this.flags = 0;
		this.ciaddr = 0;
		this.yiaddr = 0;
		this.siaddr = 0;
		this.giaddr = 0;
	}

	/**
	 * Instantiates a new DHCP packet.
	 *
	 * @param packet the packet
	 */
	DHCPPacket(byte[] packet) {
		this.initFromBytes(packet);
	}

	/**
	 * Adds the option.
	 *
	 * @param option the option
	 */
	public void addOption(DHCPOption option) {
		if (option.getType() == DHCPOption.DHCPOptionType.DHCPMessage) {
			this.setDHCPType(DHCPMessageType.convertID(option.getData()[0]));
		}
		this.options.put(option.getType().getID(), option);
	}

	/**
	 * Gets the chaddr.
	 *
	 * @return the chaddr
	 */
	public byte[] getChaddr() {
		return chaddr;
	}

	/**
	 * Gets the ciaddr.
	 *
	 * @return the ciaddr
	 */
	public int getCiaddr() {
		return ciaddr;
	}

	/**
	 * Gets the DHCP type.
	 *
	 * @return the DHCP type
	 */
	public DHCPMessageType getDHCPType() {
		return this.dhcpType;
	}

	/**
	 * Gets the file.
	 *
	 * @return the file
	 */
	public byte[] getFile() {
		return file;
	}

	/**
	 * Gets the flags.
	 *
	 * @return the flags
	 */
	public short getFlags() {
		return flags;
	}

	/**
	 * Gets the giaddr.
	 *
	 * @return the giaddr
	 */
	public int getGiaddr() {
		return giaddr;
	}

	/**
	 * Gets the hlen.
	 *
	 * @return the hlen
	 */
	public byte getHlen() {
		return hlen;
	}

	/**
	 * Gets the hops.
	 *
	 * @return the hops
	 */
	public byte getHops() {
		return hops;
	}

	/**
	 * Gets the htype.
	 *
	 * @return the htype
	 */
	public HWAddressType getHtype() {
		return htype;
	}

	/**
	 * Gets the op.
	 *
	 * @return the op
	 */
	public OpcodeType getOp() {
		return op;
	}

	/**
	 * Gets the option.
	 *
	 * @param id the id
	 * @return the option
	 */
	public DHCPOption getOption(byte id) {
		return this.options.get(id);
	}

	/**
	 * Gets the options.
	 *
	 * @return the options
	 */
	public HashMap<Byte, DHCPOption> getOptions() {
		return this.options;
	}

	/**
	 * Gets the secs.
	 *
	 * @return the secs
	 */
	public short getSecs() {
		return secs;
	}

	/**
	 * Gets the siaddr.
	 *
	 * @return the siaddr
	 */
	public int getSiaddr() {
		return siaddr;
	}

	/**
	 * Gets the sname.
	 *
	 * @return the sname
	 */
	public byte[] getSname() {
		return sname;
	}

	/**
	 * Gets the xid.
	 *
	 * @return the xid
	 */
	public int getXid() {
		return xid;
	}

	/**
	 * Gets the yiaddr.
	 *
	 * @return the yiaddr
	 */
	public int getYiaddr() {
		return yiaddr;
	}

	/**
	 * Inits the from bytes.
	 *
	 * @param packet the packet
	 */
	public void initFromBytes(byte[] packet) {
		ByteArrayInputStream bais = new ByteArrayInputStream(packet);
		DataInputStream dis = new DataInputStream(bais);

		try {
			this.setOp(dis.readByte());
			this.setHtype(dis.readByte());
			this.setHlen(dis.readByte());
			this.setHops(dis.readByte());
			this.setXid(dis.readInt());
			this.setSecs(dis.readShort());
			this.setFlags(dis.readShort());

			this.setCiaddr(dis.readInt());
			this.setYiaddr(dis.readInt());
			this.setSiaddr(dis.readInt());
			this.setGiaddr(dis.readInt());
			byte[] chaddrTemp = new byte[16];
			dis.readFully(chaddrTemp);
			this.setChaddr(chaddrTemp);
			byte[] snameTemp = new byte[64];
			dis.readFully(snameTemp);
			this.setSname(snameTemp);
			byte[] fileTemp = new byte[128];
			dis.readFully(fileTemp);
			this.setFile(fileTemp);
			dis.readInt(); // magic cookie
			byte optionID = dis.readByte();
			while ((optionID & 0xFF) != 0xFF) {
				if ((optionID & 0xFF) == 0) {
					optionID = dis.readByte();
					continue;
				}

				byte length = dis.readByte();
				byte[] data = new byte[length];
				dis.readFully(data);
				DHCPOption option = new DHCPOption(optionID, length, data);
				this.addOption(option);
				optionID = dis.readByte();
			}
			dis.close();

			DHCPOption overload = this.getOption(DHCPOptionType.OptionOverload.getID());
			if (overload != null) {
				if ((overload.getData()[0] & 0x01) == 0x01) {
					bais = new ByteArrayInputStream(this.getFile());
					dis = new DataInputStream(bais);
					optionID = dis.readByte();
					while ((optionID & 0xFF) != 0xFF) {
						if ((optionID & 0xFF) == 0) {
							optionID = dis.readByte();
							continue;
						}

						byte length = dis.readByte();
						byte[] data = new byte[length];
						dis.readFully(data);
						DHCPOption option = new DHCPOption(optionID, length, data);
						this.addOption(option);
						optionID = dis.readByte();
					}
					dis.close();
				}
				if ((overload.getData()[0] & 0x02) == 0x02) {
					bais = new ByteArrayInputStream(this.getSname());
					dis = new DataInputStream(bais);
					optionID = dis.readByte();
					while ((optionID & 0xFF) != 0xFF) {
						if ((optionID & 0xFF) == 0) {
							optionID = dis.readByte();
							continue;
						}

						byte length = dis.readByte();
						byte[] data = new byte[length];
						dis.readFully(data);
						DHCPOption option = new DHCPOption(optionID, length, data);
						this.addOption(option);
						optionID = dis.readByte();
					}
					dis.close();
				}
			}

		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	/**
	 * Options to byte.
	 *
	 * @return the byte[]
	 */
	private byte[] optionsToByte() {
		if (this.getOptions().isEmpty()) {
			return new byte[0];
		}
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		DataOutputStream dos = new DataOutputStream(baos);

		try {
			dos.write(DHCPPacket.magicCookie);
			for (DHCPOption option : this.getOptions().values()) {
				dos.write(option.getType().getID());
				dos.write(option.getLength());
				dos.write(option.getData());
			}
			dos.writeByte(DHCPOption.DHCPOptionType.End.getID());
		} catch (IOException e) {
			e.printStackTrace();
		}
		return baos.toByteArray();

	}

	/**
	 * Removes the option.
	 *
	 * @param option the option
	 */
	public void removeOption(DHCPOption option) {
		this.options.remove(option.getType().getID());
	}

	/**
	 * Removes the option.
	 *
	 * @param optionType the option type
	 */
	public void removeOption(DHCPOption.DHCPOptionType optionType) {
		this.options.remove(optionType.getID());
	}

	/**
	 * Reset options.
	 */
	public void resetOptions() {
		this.options.clear();
	}

	/**
	 * Sets the chaddr.
	 *
	 * @param chaddr the new chaddr
	 */
	public void setChaddr(byte[] chaddr) {
		if (chaddr.length < 16) {
			System.arraycopy(chaddr, 0, this.chaddr, 0, chaddr.length);
		} else if (chaddr.length == 16) {
			this.chaddr = chaddr;
		} else {
			throw new RuntimeException("chaddr too long");
		}
	}

	/**
	 * Sets the ciaddr.
	 *
	 * @param ciaddr the new ciaddr
	 */
	public void setCiaddr(int ciaddr) {
		this.ciaddr = ciaddr;
	}

	/**
	 * Sets the DHCP type.
	 *
	 * @param dhcpType the new DHCP type
	 */
	public void setDHCPType(DHCPMessageType dhcpType) {
		this.dhcpType = dhcpType;
	}

	/**
	 * Sets the file.
	 *
	 * @param file the new file
	 */
	public void setFile(byte[] file) {
		this.file = file;
	}

	/**
	 * Sets the flags.
	 *
	 * @param flags the new flags
	 */
	public void setFlags(int flags) {
		this.setFlags((short) flags);
	}

	/**
	 * Sets the flags.
	 *
	 * @param flags the new flags
	 */
	public void setFlags(short flags) {
		this.flags = flags;
	}

	/**
	 * Sets the giaddr.
	 *
	 * @param giaddr the new giaddr
	 */
	public void setGiaddr(int giaddr) {
		this.giaddr = giaddr;
	}

	/**
	 * Sets the hlen.
	 *
	 * @param hlen the new hlen
	 */
	public void setHlen(byte hlen) {
		this.hlen = hlen;
	}

	/**
	 * Sets the hlen.
	 *
	 * @param hlen the new hlen
	 */
	public void setHlen(int hlen) {
		this.setHlen((byte) hlen);
	}

	/**
	 * Sets the hops.
	 *
	 * @param hops the new hops
	 */
	public void setHops(byte hops) {
		this.hops = hops;
	}

	/**
	 * Sets the hops.
	 *
	 * @param hops the new hops
	 */
	public void setHops(int hops) {
		this.setHops((byte) hops);
	}

	/**
	 * Sets the htype.
	 *
	 * @param readByte the new htype
	 */
	private void setHtype(byte readByte) {
		this.setHtype(HWAddressType.convertID(readByte));
	}

	/**
	 * Sets the htype.
	 *
	 * @param htype the new htype
	 */
	public void setHtype(HWAddressType htype) {
		this.htype = htype;
	}

	/**
	 * Sets the op.
	 *
	 * @param readByte the new op
	 */
	private void setOp(byte readByte) {
		this.setOp(OpcodeType.convertID(readByte));

	}

	/**
	 * Sets the op.
	 *
	 * @param op the new op
	 */
	public void setOp(OpcodeType op) {
		this.op = op;
	}

	/**
	 * Sets the secs.
	 *
	 * @param secs the new secs
	 */
	public void setSecs(int secs) {
		this.setSecs((short) secs);
	}

	/**
	 * Sets the secs.
	 *
	 * @param secs the new secs
	 */
	public void setSecs(short secs) {
		this.secs = secs;
	}

	/**
	 * Sets the siaddr.
	 *
	 * @param siaddr the new siaddr
	 */
	public void setSiaddr(int siaddr) {
		this.siaddr = siaddr;
	}

	/**
	 * Sets the sname.
	 *
	 * @param sname the new sname
	 */
	public void setSname(byte[] sname) {
		this.sname = sname;
	}

	/**
	 * Sets the xid.
	 *
	 * @param xid the new xid
	 */
	public void setXid(int xid) {
		this.xid = xid;
	}

	/**
	 * Sets the yiaddr.
	 *
	 * @param yiaddr the new yiaddr
	 */
	public void setYiaddr(int yiaddr) {
		this.yiaddr = yiaddr;
	}

	/**
	 * To bytes.
	 *
	 * @return the byte[]
	 */
	public byte[] toBytes() {
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		DataOutputStream dos = new DataOutputStream(baos);

		try {
			dos.writeByte(this.getOp().id);
			dos.writeByte(this.getHtype().id);
			dos.writeByte(this.getHlen());
			dos.writeByte(this.getHops());
			dos.writeInt(this.getXid());
			dos.writeShort(this.getSecs());
			dos.writeShort(this.getFlags());
			dos.writeInt(this.getCiaddr());
			dos.writeInt(this.getYiaddr());
			dos.writeInt(this.getSiaddr());
			dos.writeInt(this.getGiaddr());
			dos.write(this.getChaddr());
			dos.write(this.getSname());
			dos.write(this.getFile());
			dos.write(optionsToByte());

		} catch (IOException e) {
			e.printStackTrace();
		}
		return baos.toByteArray();

	}

	/*
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();

		sb.append(super.toString());
		sb.append("\nDHCP Message = ");
		sb.append(this.getDHCPType());
		sb.append("\n");
		sb.append("op      =[").append(op).append("]\n");
		sb.append("htype   =[").append(htype).append("]\n");
		sb.append("hlen    =[").append(hlen & 0xFF).append("]\n");
		sb.append("hops    =[").append(hops & 0xFF).append("]\n");
		sb.append("xid     =[0x").append(Integer.toHexString(xid)).append("]\n");
		sb.append("secs    =[").append(secs).append("]\n");
		sb.append("flags   =[0x").append(Integer.toHexString(Short.toUnsignedInt(flags))).append("]\n");
		sb.append("ciaddr  =[").append(intToIPString(ciaddr)).append("] Client IP address\n");
		sb.append("yiaddr  =[").append(intToIPString(yiaddr)).append("] Your 'Client' IP address\n");
		sb.append("siaddr  =[").append(intToIPString(siaddr)).append("] Server IP address\n");
		sb.append("giaddr  =[").append(intToIPString(giaddr)).append("] Relay agent IP address\n");
		sb.append("chaddr  =[").append(byteToMac(chaddr)).append("] MAC address\n");
		sb.append("sname   =[").append(byteToString(sname)).append("]\n");
		sb.append("file    =[").append(byteToString(file)).append("]\n");
		sb.append("options =[").append(this.getOptions().values().stream().map(DHCPOption::toString)
				.collect(Collectors.joining("]\n         ["))).append("]\n");

		return sb.toString();
	}
	
	/*
	 * indented toString
	 */
	public String toTabbedString(String tab) {
		//if (true) return "";
		StringBuilder sb = new StringBuilder();

		sb.append(tab).append(super.toString()).append("\n");
		sb.append(tab).append("DHCP Message = ").append(this.getDHCPType());
		sb.append(tab).append("\n");
		sb.append(tab).append("op      =[").append(op).append("]\n");
		sb.append(tab).append("htype   =[").append(htype).append("]\n");
		sb.append(tab).append("hlen    =[").append(hlen & 0xFF).append("]\n");
		sb.append(tab).append("hops    =[").append(hops & 0xFF).append("]\n");
		sb.append(tab).append("xid     =[0x").append(Integer.toHexString(xid)).append("]\n");
		sb.append(tab).append("secs    =[").append(secs).append("]\n");
		sb.append(tab).append("flags   =[0x").append(Integer.toHexString(Short.toUnsignedInt(flags))).append("]\n");
		sb.append(tab).append("ciaddr  =[").append(intToIPString(ciaddr)).append("] Client IP address\n");
		sb.append(tab).append("yiaddr  =[").append(intToIPString(yiaddr)).append("] Your 'Client' IP address\n");
		sb.append(tab).append("siaddr  =[").append(intToIPString(siaddr)).append("] Server IP address\n");
		sb.append(tab).append("giaddr  =[").append(intToIPString(giaddr)).append("] Relay agent IP address\n");
		sb.append(tab).append("chaddr  =[").append(byteToMac(chaddr).substring(0, 17)).append("] MAC address\n");
		sb.append(tab).append("sname   =[").append(byteToString(sname)).append("]\n");
		sb.append(tab).append("file    =[").append(byteToString(file)).append("]\n");
		sb.append(tab).append("options =[").append(this.getOptions().values().stream().map(DHCPOption::toString)
				.collect(Collectors.joining("]\n"+tab+"         ["))).append("]\n");

		return sb.toString();
	}

}
